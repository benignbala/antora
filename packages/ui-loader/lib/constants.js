'use strict'

module.exports = Object.freeze({
  UI_CACHE_PATH: '.antora-cache/ui',
  UI_CONFIG_FILENAME: 'ui.yml',
})
