'use strict'

/**
 * Document Converter component for Antora
 *
 * Using the AsciiDoc Loader, converts the AsciiDoc contents on files to
 * embeddable HTML and assigns AsciiDoc-related metadata, such as page
 * attributes, to the file.
 *
 * @namespace document-converter
 */
module.exports = require('./convert-document')
